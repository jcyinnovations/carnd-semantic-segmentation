import os
import argparse
import warnings
import tensorflow as tf
import helper
from distutils.version import LooseVersion
from os.path import join, expanduser
import project_tests as tests

# Check TensorFlow Version
assert LooseVersion(tf.__version__) >= LooseVersion('1.0'),\
    'Please use TensorFlow version 1.0 or newer.  You are using {}'.format(tf.__version__)
print('TensorFlow Version: {}'.format(tf.__version__))

# Check for a GPU
if not tf.test.gpu_device_name():
    warnings.warn('No GPU found. Please use a GPU to train your neural network.')
else:
    print('Default GPU Device: {}'.format(tf.test.gpu_device_name()))


def load_vgg(sess, vgg_path):
    """
    Load Pretrained VGG Model into TensorFlow.

    :param sess: TensorFlow Session
    :param vgg_path: Path to vgg folder, containing "variables/" and "saved_model.pb"
    :return: Tuple of Tensors from VGG model (image_input, keep_prob, layer3_out, layer4_out, layer7_out)
    """

    vgg_input_tensor_name = 'image_input:0'
    vgg_keep_prob_tensor_name = 'keep_prob:0'
    vgg_layer3_out_tensor_name = 'layer3_out:0'
    vgg_layer4_out_tensor_name = 'layer4_out:0'
    vgg_layer7_out_tensor_name = 'layer7_out:0'

    tf.saved_model.loader.load(sess, ['vgg16'], vgg_path)
    graph = tf.get_default_graph()

    image_input = graph.get_tensor_by_name(vgg_input_tensor_name)
    keep_prob = graph.get_tensor_by_name(vgg_keep_prob_tensor_name)
    layer3_out = graph.get_tensor_by_name(vgg_layer3_out_tensor_name)
    layer4_out = graph.get_tensor_by_name(vgg_layer4_out_tensor_name)
    layer7_out = graph.get_tensor_by_name(vgg_layer7_out_tensor_name)

    return image_input, keep_prob, layer3_out, layer4_out, layer7_out


def layers(vgg_layer3_out, vgg_layer4_out, vgg_layer7_out, num_classes):
    """
    Create the layers for a fully convolutional network.  Build skip-layers using the vgg layers.
    For reference: https://people.eecs.berkeley.edu/~jonlong/long_shelhamer_fcn.pdf

    :param vgg_layer7_out: TF Tensor for VGG Layer 3 output
    :param vgg_layer4_out: TF Tensor for VGG Layer 4 output
    :param vgg_layer3_out: TF Tensor for VGG Layer 7 output
    :param num_classes: Number of classes to classify
    :return: The Tensor for the last layer of output
    """

    kreg  = tf.contrib.layers.l2_regularizer(0.5)
    kinit = tf.keras.initializers.he_normal()
    #kinit = None
    
    # Reduce channels to fit road/not-road required output
    layer3 = tf.layers.conv2d(vgg_layer3_out, num_classes, kernel_size=[1, 1], padding='same', kernel_initializer=kinit, kernel_regularizer=kreg)
    layer4 = tf.layers.conv2d(vgg_layer4_out, num_classes, kernel_size=[1, 1], padding='same', kernel_initializer=kinit, kernel_regularizer=kreg)
    layer7 = tf.layers.conv2d(vgg_layer7_out, num_classes, kernel_size=[1, 1], padding='same', kernel_initializer=kinit, kernel_regularizer=kreg)

    # Add skip connections from layers 3, 4, and 7
    layer7_scaled = tf.image.resize_images(layer7, size=[10, 36])
    combined_47 = tf.add(layer7_scaled, layer4)
    combined_47_scaled = tf.image.resize_images(combined_47, size=[20, 72])
    combined_347 = tf.add(layer3, combined_47_scaled)

    # resize to original size
    combined_347_scaled = tf.image.resize_images(combined_347, size=[160, 576])
    combined_347_scaled = tf.layers.conv2d(combined_347_scaled, num_classes, kernel_size=[15, 15], padding='same', kernel_regularizer=kreg)

    return combined_347_scaled


def optimize(nn_output, labels, learning_rate, num_classes):
    """
    Build the TensorFLow loss and optimizer operations.
    :param nn_output: TF Tensor of the last layer in the neural network
    :param labels: TF Placeholder for the correct label image
    :param learning_rate: TF Placeholder for the learning rate
    :param num_classes: Number of classes to classify
    :return: Tuple of (logits, train_op, cross_entropy_loss)
    """
    # Flatten labels and output before applying cross entropy
    logits = tf.reshape(nn_output, (-1, num_classes))
    labels = tf.reshape(labels, (-1, num_classes))
    # Define loss
    cross_entropy_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=labels, logits=logits))
    # Define optimization step
    training_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cross_entropy_loss)
    return logits, training_op, cross_entropy_loss

def train_nn(sess, epochs, batch_size, get_batches_fn, train_op, cross_entropy_loss, img, labels, keep_prob, learning_rate):
    """
    Train neural network and print out the loss during training.
    :param sess: TF Session
    :param epochs: Number of epochs
    :param batch_size: Batch size
    :param get_batches_fn: Function to get batches of training data.  Call using get_batches_fn(batch_size)
    :param train_op: TF Operation to train the neural network
    :param cross_entropy_loss: TF Tensor for the amount of loss
    :param img: TF Placeholder for input images
    :param labels: TF Placeholder for label images
    :param keep_prob: TF Placeholder for dropout keep probability
    :param learning_rate: TF Placeholder for learning rate
    """

    sess.run(tf.global_variables_initializer())
    lr = 0.0001
    repeats = 100
    
    for e in range(0, epochs):
        epoch_loss = 0.0
        for i in range(0, repeats):
            # Load a batch of examples
            batch_x, batch_y = next(get_batches_fn(batch_size))
            _, loss = sess.run(fetches=[train_op, cross_entropy_loss], feed_dict={img: batch_x, labels: batch_y, keep_prob: 0.50, learning_rate: lr})
            epoch_loss += loss

        print('Epoch: {:02d}, Loss: {:.03f}'.format(e, epoch_loss/repeats))


def perform_tests():
    tests.test_for_kitti_dataset(data_dir)
    tests.test_load_vgg(load_vgg, tf)
    tests.test_layers(layers)
    tests.test_optimize(optimize)
    tests.test_train_nn(train_nn)


def run():
    num_classes = 2
    data_dir = './data'
    runs_dir = './runs'
    h, w = (160, 576)
    batch_size = 8
    epochs = 10
    
    with tf.Session() as sess:
        vgg_path = join(data_dir, 'vgg')
        gen = helper.gen_batch_function(join(data_dir, 'data_road/training'), (h, w))
        input_img, keep_prob, vgg_layer3_out, vgg_layer4_out, vgg_layer7_out = load_vgg(sess, vgg_path)
        model_output = layers(vgg_layer3_out, vgg_layer4_out, vgg_layer7_out, num_classes)
        labels = tf.placeholder(tf.float32, shape=[None, h, w, num_classes])
        learning_rate = tf.placeholder(tf.float32, shape=[])
        logits, train_op, cross_entropy_loss = optimize(model_output, labels, learning_rate, num_classes)
        train_nn(sess, epochs, batch_size, gen, train_op, cross_entropy_loss, input_img, labels, keep_prob, learning_rate)

        helper.save_inference_samples(runs_dir, data_dir, sess, (h, w), logits, keep_prob, input_img)

if __name__ == '__main__':
    #Using a multi-gpu server so have to switch off all but one GPU for simplicity
    os.environ['CUDA_VISIBLE_DEVICES'] = "0"
    run()
