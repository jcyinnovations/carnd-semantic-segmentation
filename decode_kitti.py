'''
Created on Sep 9, 2017

@author: jyarde
'''
import cv2
import numpy as np

KITTI_XLATE = {
    0: 0,
    1: 1,
    2: 101
}

KITTI_PALETTE = {
    0: (0,   0, 0),
    1: (255, 0, 0),
    2: (255, 0, 255)
}

KITTI_LABELS = {"opposing": 0, "background": 1, "road": 2}

TARGET_CLASS_NAMES = ["opposing", "background", "road"]

kitti_target_classes = len(TARGET_CLASS_NAMES)

def decode(label):
    code = np.zeros(label.shape)
    label = label/255
    code[:,:,0] = label[:,:,0] + label[:,:,1]*10 + label[:,:,2]*100
    #code = code.astype("uint8")
    decoded = np.zeros(label.shape[:2]+(kitti_target_classes,) )
    for i in range(kitti_target_classes):
        layer = np.zeros(label.shape[:2])
        # Needed because Keras ImageGenerator reorders classes alphanumerically
        classid = KITTI_LABELS[TARGET_CLASS_NAMES[i]]
        layer[code[:,:,0]==KITTI_XLATE[classid]] = 1
        decoded[:,:,i] = layer
    return decoded

def decode_class(label, classid=None):
    code = np.zeros(label.shape)
    label = label/64
    code[:,:,0] = label[:,:,0] + label[:,:,1]*10 + label[:,:,2]*100
    #print("Unique values", np.unique(code  a))
    layer = np.zeros(label.shape[:2])
    target_class = TARGET_CLASS_NAMES[classid]
    kitti_classid = KITTI_LABELS[target_class]
    #print("Target Class", classid, target_class, voc_classid, PASCAL_XLATE[voc_classid], label.shape)
    layer[code[:,:,0]==KITTI_XLATE[kitti_classid]] = 1
    return layer
