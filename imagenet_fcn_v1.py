from keras import backend as K
from keras.applications.mobilenet import MobileNet, relu6, DepthwiseConv2D
from keras.models import load_model, Model, Sequential
from keras.optimizers import RMSprop, Adam, SGD
from keras.engine.topology import Layer
from keras.engine import InputSpec

from keras.layers import Input, Activation, Conv2D
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import GlobalAveragePooling2D, MaxPooling2D
from keras.layers.convolutional import Conv2DTranspose, UpSampling2D
from keras.layers.core import Reshape, Dropout
from keras.layers.core import Lambda

import tensorflow as tf

import numpy as np

import keras

class CustomReshape(Layer):

    def __init__(self, output_dim, **kwargs):
        self.output_dim = output_dim
        super(CustomReshape, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        super(CustomReshape, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        return tf.reshape(x, [-1, self.output_dim]) #np.reshape(x, (-1, 14)) 

    def compute_output_shape(self, input_shape):
        print("CustomReshape", input_shape)
        return (input_shape[0], self.output_dim)

    def get_config(self):
        config = {'output_dim': self.output_dim}
        base_config = super(CustomReshape, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class BilinearUpSampling2D(Layer):

    def __init__(self, output_dim, scale_factor, **kwargs):
        self.output_dim   = output_dim
        self.scale_factor = scale_factor
        super(BilinearUpSampling2D, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable weight variable for this layer.
        super(BilinearUpSampling2D, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, x):
        input_size = tf.shape(x)
        size = (self.scale_factor*input_size[1], self.scale_factor*input_size[2])
        new_size = tf.convert_to_tensor(size, dtype=tf.int32)
        return tf.image.resize_images(x, new_size)
        #return tf.reshape(x, [-1, self.output_dim]) 

    def compute_output_shape(self, input_shape):
        print("BilinearUpSampling2D", input_shape)
        return input_shape[:-1] + (self.output_dim,)

    def get_config(self):
        config = {'output_dim': self.output_dim, 'scale_factor': self.scale_factor}
        base_config = super(BilinearUpSampling2D, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

def generate_2class_skiplayer_fcn(source_model, classes=2, deconv=True, dropout=1e-3):
    '''
        Make Mobilenet fully convolutional with any size input and
        segmentation output. source_model provides fully loaded Mobilenet 
        so we can extract its weights for the FCN model
        
        Return the fully convolutional model.
    '''
    x = source_model.layers[-1].output #Last layer used from Mobilenet. Choosing the last convolution
    #x_channels = mobilenet.layers[-7].output_shape[-1]
    channel_axis = 1 if K.image_data_format() == 'channels_first' else -1

    '''    
    for i in range(len(source_model.layers) - 4):
        source_model.layers[i].trainable = False
    source_model.layers[-3].trainable = True
    trainable_layers = 16 # Train the deconvolutions
    '''

    bias_flag = True
    bias_init = 'he_normal'

    skip_id = 15
    skip_7 = source_model.layers[-skip_id].output
    skip_7_channels = source_model.layers[-skip_id].output_shape[-1]
    skip_7 = Conv2D(classes, (1,1),
               padding="same",
               use_bias=bias_flag,
               strides=(1,1),
               name='t_skip_conv_7',
               kernel_initializer='he_normal',
               bias_initializer=bias_init)(skip_7)
    
    skip_id = 57
    skip_5 = source_model.layers[-skip_id].output
    skip_5_channels = source_model.layers[-skip_id].output_shape[-1]
    skip_5 = Conv2D(classes, (1,1),
               padding="same",
               use_bias=bias_flag,
               strides=(1,1),
               name='t_skip_conv_5',
               kernel_initializer='he_normal',
               bias_initializer=bias_init)(skip_5)
    
    skip_id = 71
    skip_3 = source_model.layers[-skip_id].output
    skip_3_channels = source_model.layers[-skip_id].output_shape[-1]
    skip_3 = Conv2D(classes, (1,1),
               padding="same",
               use_bias=bias_flag,
               strides=(1,1),
               name='t_skip_conv_3',
               kernel_initializer='he_normal',
               bias_initializer=bias_init)(skip_3)
    
    sig = Activation('softmax', name='act_softmax2')
    bias_flag = True
    bias_init = 'he_normal'
    
    if deconv:
        block_id = 1
        x = Conv2DTranspose(classes, 
                            (3, 3), 
                            padding='same', 
                            activation='softmax',
                            use_bias=False, 
                            strides=(2,2), 
                            name='deconv_%d' % block_id, 
                            kernel_initializer='he_normal', 
                            bias_initializer=bias_init)(x)
        x = BatchNormalization(axis=channel_axis, name='deconv_bn_%d' % block_id)(x)
        block_id = 2
        x_skip7 = keras.layers.Add()([x, skip_7])
        x = Conv2DTranspose(classes, 
                            (3, 3), 
                            padding='same', 
                            activation='softmax',
                            use_bias=bias_flag, 
                            strides=(2,2), 
                            name='deconv_%d' % block_id, 
                            kernel_initializer='he_normal', 
                            bias_initializer=bias_init)(x_skip7)
        x = BatchNormalization(axis=channel_axis, name='deconv_bn_%d' % block_id)(x)
        block_id = 3
        x_skip5 = keras.layers.Add()([x, skip_5])
        x = Conv2DTranspose(classes, 
                            (3, 3), 
                            padding='same', 
                            activation='softmax',
                            use_bias=bias_flag, 
                            strides=(2,2), 
                            name='deconv_%d' % block_id, 
                            kernel_initializer='he_normal', 
                            bias_initializer=bias_init)(x_skip5)
        x = BatchNormalization(axis=channel_axis, name='deconv_bn_%d' % block_id)(x)
        block_id = 4
        x_skip3 = keras.layers.Add()([x, skip_3])
        x = Conv2DTranspose(classes, 
                            (3, 3), 
                            padding='same',
                            activation='softmax',
                            use_bias=bias_flag, 
                            strides=(2,2), 
                            name='deconv_%d' % block_id, 
                            kernel_initializer='he_normal', 
                            bias_initializer=bias_init)(x_skip3)
        x = BatchNormalization(axis=channel_axis, name='deconv_bn_%d' % block_id)(x)
        block_id = 5
        x = Conv2DTranspose(classes, 
                            (3, 3), 
                            padding='same', 
                            activation='softmax',
                            use_bias=bias_flag, 
                            strides=(2,2), 
                            name='deconv_%d' % block_id, 
                            kernel_initializer='he_normal', 
                            bias_initializer=bias_init)(x)
    else:
        x = Conv2D(classes, (1,1),
                   padding="same",
                   use_bias=bias_flag,
                   strides=(1,1),
                   activation='softmax',
                   name='t_reduce_channels_1',
                   kernel_initializer='he_normal',
                   bias_initializer=bias_init)(x)
        x = BilinearUpSampling2D(scale_factor=2, output_dim=classes, name='upscore_1')(x)
        x_skip7 = keras.layers.Add()([x, skip_7])
        x = Conv2D(classes, (1,1),
                   padding="same",
                   use_bias=bias_flag,
                   strides=(1,1),
                   activation='softmax',
                   name='t_conv2d_smoothing_1',
                   kernel_initializer='he_normal',
                   bias_initializer=bias_init)(x_skip7)
        
        x = BilinearUpSampling2D(scale_factor=2, output_dim=classes, name='upscore_2')(x)
        x_skip5 = keras.layers.Add()([x, skip_5])
        x = Conv2D(classes, (1,1),
                   padding="same",
                   use_bias=bias_flag,
                   strides=(1,1),
                   activation='softmax',
                   name='t_conv2d_smoothing_2',
                   kernel_initializer='he_normal',
                   bias_initializer=bias_init)(x_skip5)
        
        x = BilinearUpSampling2D(scale_factor=2, output_dim=classes, name='upscore_3')(x)
        x_skip3 = keras.layers.Add()([x, skip_3])
        x = Conv2D(classes, (1,1),
                   padding="same",
                   use_bias=bias_flag,
                   strides=(1,1),
                   activation='softmax',
                   name='t_conv2d_smoothing_3',
                   kernel_initializer='he_normal',
                   bias_initializer=bias_init)(x_skip3)
        
        x = BilinearUpSampling2D(scale_factor=2, output_dim=classes, name='upscore_4')(x)
        x = Conv2D(classes, (1,1),
                   padding="same",
                   use_bias=bias_flag,
                   strides=(1,1),
                   activation='softmax',
                   name='t_conv2d_smoothing_4',
                   kernel_initializer='he_normal',
                   bias_initializer=bias_init)(x)
        
        x = BilinearUpSampling2D(scale_factor=2, output_dim=classes, name='upscore_5')(x)
        x = Conv2D(classes, (1,1),
                   padding="same",
                   use_bias=bias_flag,
                   strides=(1,1),
                   activation='softmax',
                   name='t_conv2d_smoothing_5',
                   kernel_initializer='he_normal',
                   bias_initializer=bias_init)(x)
    #
    #compose the combined model with the new top
    new_model = Model(source_model.input, x)
    for i in range(len(new_model.layers)-22):
        new_model.layers[i].trainable = False
    new_model.compile(loss='mean_squared_error', optimizer=Adam(lr=0.001), metrics=['accuracy'])
    new_model.summary()
    return new_model

